# Apis Sumas
Test simple spring-boot api
## Requirements
- JDK 11
- Maven 3.8.6
- Postgres database 12.11
## Before development
- you can use docker database from: https://hub.docker.com/repository/docker/jcocina/bd-sumas-apis_postgres/general
- otherwise you can use sql script **"database-script.sql"** to set up you own database
- Database connection pointing to **127.0.0.1:15432**
## Development
1. Run `mvn install` for update maven dependencies
2. Run `mvn sprig-boot:run` for dev server. Api run on [http://localhost:8090/](http://localhost:8090/)
## Documentation
When api is running you can see methods examples for Postman on file **"APIS SUMAS.postman_collection.json"**
## Methods
- **SignUp (register)**
  - **Type**: POST  
  - **uri**: /users/signup
  - **Json Body**:  {"username" : "newuser", "password" : "newuserpassword"}
- **SignIn (login)**
  - **Type**: POST
  - **uri**: /users/signin
  - **Json Body**:  {"username" : "newuser", "password" : "newuserpassword"}
- **Addition (operation of addition)**
  - **Type**: GET
  - **uri**: /operations/addition
  - **URL Params**:  ?a=1.3&b=10.7
- **Movement (paginated list of movements)**
  - **Type**: GET
  - **uri**: http://localhost:8090/movements/list
  - **URL Params**:  ?page=0