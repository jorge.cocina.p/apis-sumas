FROM openjdk:11.0-jdk-slim-stretch

VOLUME [ "/home" ]
COPY target/apis-sumas-1.0-SNAPSHOT.jar apis-sumas-1.0-SNAPSHOT.jar
EXPOSE 8090
ENTRYPOINT [ "java", "-jar", "/apis-sumas-1.0-SNAPSHOT.jar" ]