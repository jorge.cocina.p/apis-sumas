package cl.jc.sumas.dto;

import lombok.Data;

import java.util.List;

@Data
public class SessionDto {

    String token;
    Integer id;
    List<String> roles;


    @Override
    public String toString() {
        return "{" +
                "\"token\":\"" + token + "\"" +
                ", \"id\":\"" + id + "\"" +
                ", \"roles\":\"" + roles.toString() + "\"" +
                '}';
    }

}
