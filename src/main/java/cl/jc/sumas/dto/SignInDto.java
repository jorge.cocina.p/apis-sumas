package cl.jc.sumas.dto;

import lombok.Data;

@Data
public class SignInDto {

    String username;
    String password;

    @Override
    public String toString() {
        return "{" +
                "\"username\":\"" + username + "\"" +
                ", \"password\":null" +
                '}';
    }
}
