package cl.jc.sumas.dto;

import lombok.Data;

@Data
public class OperationResponseDto {

    String operation;
    String result;

    @Override
    public String toString() {
        return "{" +
                "\"operation\":\"" + operation + "\"" +
                ", \"result\":\"" + result + "\"" +
                '}';
    }

}
