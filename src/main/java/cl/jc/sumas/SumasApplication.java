package cl.jc.sumas;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.ComponentScan;

@SpringBootApplication
@ComponentScan("cl.jc.sumas")
public class SumasApplication {

    public static void main(String[] args) {
        SpringApplication.run(SumasApplication.class, args);
    }
}
