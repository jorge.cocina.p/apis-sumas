package cl.jc.sumas.controller;

import cl.jc.sumas.configuration.ContextProcess;
import cl.jc.sumas.dto.OperationResponseDto;
import cl.jc.sumas.exception.BadRequestException;
import cl.jc.sumas.service.MovementService;
import cl.jc.sumas.service.OperationService;
import cl.jc.sumas.utils.Constants;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@RestController
@CrossOrigin(origins = "*")
@RequestMapping("/operations")
@Slf4j
public class OperationsController {

    @Autowired
    OperationService operationService;

    @Autowired
    MovementService movementService;

    @GetMapping("/addition")
    @PreAuthorize("hasAuthority('user') or hasAuthority('admin')")
    public ResponseEntity<OperationResponseDto> userAccess(@RequestParam(name = "a") String a, @RequestParam(name = "b") String b) throws BadRequestException {
        ContextProcess.setCalledUrl("/operations/addition");
        ContextProcess.setCalledParams("{\"a\":\""+a+",\"b\":\""+b+"\"}");
        OperationResponseDto responseDto = operationService.addition(a, b);
        movementService.saveMovement(responseDto.toString(), Constants.VAL_MAP_SUCCESS);
        return new ResponseEntity<>(responseDto, HttpStatus.OK);

    }

}
