package cl.jc.sumas.controller;

import cl.jc.sumas.configuration.ContextProcess;
import cl.jc.sumas.dto.SessionDto;
import cl.jc.sumas.dto.SignInDto;
import cl.jc.sumas.dto.SignUpDto;
import cl.jc.sumas.exception.BadRequestException;
import cl.jc.sumas.service.MovementService;
import cl.jc.sumas.service.UsersService;
import cl.jc.sumas.utils.Constants;
import com.google.gson.Gson;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.io.IOUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletRequest;
import java.io.IOException;

@RestController
@CrossOrigin(origins = "*")
@RequestMapping("/users")
@Slf4j
public class UsersController {

    @Autowired
    UsersService usersService;

    @Autowired
    MovementService movementService;

    @PostMapping(value = "/signup", consumes = { MediaType.APPLICATION_JSON_VALUE })
    public ResponseEntity<SessionDto> signUp(HttpServletRequest input) throws BadRequestException {
        ContextProcess.setCalledUrl("/users/signup");

        Gson gson = new Gson();
        SignUpDto signUpDto;
        try {
            signUpDto = gson.fromJson(IOUtils.toString(input.getReader()), SignUpDto.class);
            ContextProcess.setCalledParams(signUpDto.toString());
        } catch (IOException e) {
            throw new BadRequestException("error reading request");
        }
        log.info("Signup username {}", signUpDto.getUsername());
        SessionDto sessionDto = usersService.createUser(signUpDto);
        movementService.saveMovement(sessionDto.toString(), Constants.VAL_MAP_SUCCESS);
        return new ResponseEntity<>(sessionDto, HttpStatus.OK);
    }

    @PostMapping(value = "/signin", consumes = { MediaType.APPLICATION_JSON_VALUE })
    public ResponseEntity<SessionDto> signIn(HttpServletRequest input) throws BadRequestException {
        ContextProcess.setCalledUrl("/users/signin");
        Gson gson = new Gson();
        SignInDto signInDto;
        try {
            signInDto = gson.fromJson(IOUtils.toString(input.getReader()), SignInDto.class);
            ContextProcess.setCalledParams(signInDto.toString());
        } catch (IOException e) {
            throw new BadRequestException("error reading request");
        }
        log.info("Signin username {}", signInDto.getUsername());
        SessionDto sessionDto = usersService.loginUser(signInDto);
        movementService.saveMovement(sessionDto.toString(), Constants.VAL_MAP_SUCCESS);
        return new ResponseEntity<>(sessionDto, HttpStatus.OK);
    }

}
