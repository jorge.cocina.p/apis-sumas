package cl.jc.sumas.controller.interceptor;

import cl.jc.sumas.exception.BadRequestException;
import cl.jc.sumas.helper.MessageErrorHelper;
import cl.jc.sumas.service.MovementService;
import cl.jc.sumas.utils.Constants;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;

@Slf4j
@ControllerAdvice
public class ExceptionRestInterceptorAdvice {

    @Autowired
    MovementService movementService;

    @ExceptionHandler(BadRequestException.class)
    public ResponseEntity<String> HttpMessageHandler(BadRequestException e) {

        log.warn("BadRequestException with message {}",e.getMessage());
        String response =  MessageErrorHelper.dataRequestInvalid(e.getMessage());
        movementService.saveMovement(response, Constants.VAL_MAP_ERROR);
        return new ResponseEntity<>(response, HttpStatus.BAD_REQUEST);

    }

    @ExceptionHandler(RuntimeException.class)
    public ResponseEntity<String> RuntimeExceptionHandler(RuntimeException e) {

        log.warn("RuntimeException with message {}",e.getMessage());
        String response = "";
        if (e.getMessage().equals("Access is denied")) {
            response = MessageErrorHelper.unathorized(e.getMessage());
            movementService.saveMovement(response, Constants.VAL_MAP_ERROR);
            return new ResponseEntity<>(response, HttpStatus.UNAUTHORIZED);
        } else {
            response = MessageErrorHelper.runtime(e.getMessage());
            movementService.saveMovement(response, Constants.VAL_MAP_ERROR);
            return new ResponseEntity<>(response, HttpStatus.INTERNAL_SERVER_ERROR);
        }

    }

    @ExceptionHandler(Exception.class)
    public ResponseEntity<String> HttpMessageHandlerGeneral(Exception e) {

        log.warn("Exception with message {}",e.getMessage());
        String response =  MessageErrorHelper.dataRequestInvalid(e.getMessage());
        movementService.saveMovement(response, Constants.VAL_MAP_ERROR);
        return new ResponseEntity<>(response, HttpStatus.BAD_REQUEST);

    }

}
