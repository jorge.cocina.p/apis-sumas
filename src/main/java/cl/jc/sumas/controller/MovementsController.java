package cl.jc.sumas.controller;

import cl.jc.sumas.domain.Movement;
import cl.jc.sumas.service.MovementService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@RestController
@CrossOrigin(origins = "*")
@RequestMapping("/movements")
@Slf4j
public class MovementsController {

    @Autowired
    MovementService movementService;

    @GetMapping("/list")
    @PreAuthorize("hasAuthority('user') or hasAuthority('admin')")
    public ResponseEntity<Page<Movement>> userAccess(@RequestParam(name = "page", required = false) Integer page) {

        Page<Movement> movements = movementService.getMovements(page);
        return new ResponseEntity<>(movements, HttpStatus.OK);

    }

}
