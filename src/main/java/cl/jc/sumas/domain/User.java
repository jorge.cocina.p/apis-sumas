package cl.jc.sumas.domain;

import lombok.Data;
import org.hibernate.annotations.CreationTimestamp;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;
import java.util.Date;
import java.util.HashSet;
import java.util.Set;

@Data
@Entity
@Table(name = "users", schema = "apissumas", uniqueConstraints = {
        @UniqueConstraint(columnNames = "username")})

public class User {

    @Id
    @Column(name = "id")
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    Integer id;

    @Column(name = "username", length = 50)
    String username;

    @Column(name = "password")
    String password;

    @Column(name = "created_on")
    @CreationTimestamp
    Date createdOn;

    @Column(name = "last_login")
    Date lastLogin;

    @ManyToMany(fetch = FetchType.LAZY)
    @JoinTable(	name = "users_roles", schema = "apissumas",
            joinColumns = @JoinColumn(name = "id_user"),
            inverseJoinColumns = @JoinColumn(name = "id_role"))
    private Set<Role> roles = new HashSet<>();

}