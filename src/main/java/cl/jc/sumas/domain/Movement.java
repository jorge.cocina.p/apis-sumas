package cl.jc.sumas.domain;

import lombok.Data;
import org.hibernate.annotations.CreationTimestamp;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import java.util.Date;

@Data
@Entity
@Table(name = "movements", schema = "apissumas")
public class Movement {

    @Id
    @Column(name = "id")
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    Integer id;

    @Column(name = "username")
    String username;

    @Column(name = "status")
    String status;

    @Column(name = "endpoint")
    String endpoint;

    @Column(name = "input")
    String input;

    @Column(name = "output")
    String output;

    @Column(name = "created_on")
    @CreationTimestamp
    Date createdOn;

}