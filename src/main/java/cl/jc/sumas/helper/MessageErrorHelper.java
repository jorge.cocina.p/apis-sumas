package cl.jc.sumas.helper;

import cl.jc.sumas.utils.Constants;
import com.google.gson.GsonBuilder;

import java.util.HashMap;
import java.util.LinkedHashMap;

public final class MessageErrorHelper {

    /**
     * Instantiates a new message error helper.
     */
    private MessageErrorHelper() {
        super();
    }

    public static String dataRequestInvalid(String message) {
        HashMap<String, Object> error = new LinkedHashMap<>();
        error.put(Constants.KEY_MAP_STATUS, Constants.VAL_MAP_ERROR);
        error.put(Constants.KEY_MAP_VALUE, Constants.VAL_DATA_INVALID);
        error.put(Constants.KEY_MAP_DESCRIPTION, Constants.DSC_DATA_INVALID);
        error.put(Constants.KEY_MAP_DETAIL, (null != message ? message : Constants.EMPTY) );
        return new GsonBuilder().setPrettyPrinting().create().toJson(error);
    }

    public static String runtime(String message) {
        HashMap<String, Object> error = new LinkedHashMap<>();
        error.put(Constants.KEY_MAP_STATUS, Constants.VAL_MAP_ERROR);
        error.put(Constants.KEY_MAP_VALUE, Constants.VAL_DATA_RUNTIME);
        error.put(Constants.KEY_MAP_DESCRIPTION, Constants.DSC_DATA_RUNTIME);
        error.put(Constants.KEY_MAP_DETAIL, (null != message ? message : Constants.EMPTY) );
        return new GsonBuilder().setPrettyPrinting().create().toJson(error);
    }

    public static String unathorized(String message) {
        HashMap<String, Object> error = new LinkedHashMap<>();
        error.put(Constants.KEY_MAP_STATUS, Constants.VAL_MAP_ERROR);
        error.put(Constants.KEY_MAP_VALUE, Constants.VAL_DATA_AUTH);
        error.put(Constants.KEY_MAP_DESCRIPTION, Constants.DSC_DATA_AUTH);
        error.put(Constants.KEY_MAP_DETAIL, (null != message ? message : Constants.EMPTY) );
        return new GsonBuilder().setPrettyPrinting().create().toJson(error);
    }

    public static String general(String message) {
        HashMap<String, Object> error = new LinkedHashMap<>();
        error.put(Constants.KEY_MAP_STATUS, Constants.VAL_MAP_ERROR);
        error.put(Constants.KEY_MAP_VALUE, Constants.VAL_DATA_GENERAL);
        error.put(Constants.KEY_MAP_DESCRIPTION, Constants.DSC_DATA_GENERAL);
        error.put(Constants.KEY_MAP_DETAIL, (null != message ? message : Constants.EMPTY) );
        return new GsonBuilder().setPrettyPrinting().create().toJson(error);
    }

}
