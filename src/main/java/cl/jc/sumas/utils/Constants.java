package cl.jc.sumas.utils;

public final class Constants {

    /** Key's Constants. */
    public static final String KEY_MAP_STATUS             		= "status";
    public static final String KEY_MAP_VALUE             		= "value";
    public static final String KEY_MAP_DESCRIPTION         		= "description";
    public static final String KEY_MAP_DETAIL            		= "detail";
    public static final String VAL_MAP_SUCCESS            		= "success";
    public static final String TOKEN_PREFIX            		    = "Bearer ";
    public static final String VAL_MAP_ERROR           		    = "error";
    public static final String  VAL_DATA_INVALID				= "BadRequestException";
    public static final String  VAL_DATA_RUNTIME				= "RuntimeException";
    public static final String  VAL_DATA_AUTH				    = "AuthException";
    public static final String  VAL_DATA_GENERAL				= "Untracked Exception";
    public static final String  DSC_DATA_INVALID				= "Invalid data";
    public static final String  DSC_DATA_RUNTIME				= "Execution error";
    public static final String  DSC_DATA_AUTH				    = "You need permission to access this. Visit /users/signin";
    public static final String  DSC_DATA_GENERAL				= "Well, that's an unexpected exception";
    public static final String EMPTY           		            = "";
    public static final int PAGE_SIZE           		        = 10;
    public static final int PAGE_INIT          		            = 0;

    private Constants() {
    }

}
