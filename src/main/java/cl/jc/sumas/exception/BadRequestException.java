package cl.jc.sumas.exception;

public class BadRequestException extends Exception {

    /** The Constant serialVersionUID. */
    private static final long serialVersionUID = -2969518002634364652L;

    /**
     * Instantiates a new URI not found exception.
     */
    public BadRequestException() {
        super();
    }

    /**
     * Instantiates a new URI not found exception.
     *
     * @param message the message
     * @param cause the cause
     * @param enableSuppression the enable suppression
     * @param writableStackTrace the writable stack trace
     */
    public BadRequestException(String message, Exception cause, boolean enableSuppression, boolean writableStackTrace) {
        super(message, cause, enableSuppression, writableStackTrace);
    }

    /**
     * Instantiates a new URI not found exception.
     *
     * @param message the message
     * @param cause the cause
     */
    public BadRequestException(String message, Exception cause) {
        super(message, cause);
    }

    /**
     * Instantiates a new URI not found exception.
     *
     * @param message the message
     */
    public BadRequestException(String message) {
        super(message);
    }

    /**
     * Instantiates a new URI not found exception.
     *
     * @param cause the cause
     */
    public BadRequestException(Exception cause) {
        super(cause);
    }

}
