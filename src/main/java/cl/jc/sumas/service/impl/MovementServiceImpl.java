package cl.jc.sumas.service.impl;

import cl.jc.sumas.configuration.ContextProcess;
import cl.jc.sumas.domain.Movement;
import cl.jc.sumas.repository.MovementsRepository;
import cl.jc.sumas.service.MovementService;
import cl.jc.sumas.utils.Constants;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import java.util.Objects;

@Service
@Slf4j
public class MovementServiceImpl implements MovementService {

    @Autowired
    MovementsRepository movementsRepository;

    @Override
    public void saveMovement(String output, String status) {
        try {
            Movement movement = new Movement();

            movement.setStatus(status);
            movement.setEndpoint(ContextProcess.getCalledUrl());
            movement.setInput(ContextProcess.getCalledParams());
            movement.setOutput(output);
            movement.setUsername(ContextProcess.getCallerId());

            movementsRepository.save(movement);

        } catch (Exception e) {
            log.error("Error with message: {}", e.getMessage());
        } finally {
            ContextProcess.clear();
        }
    }

    @Override
    public Page<Movement> getMovements(Integer page) {
        Pageable pageable = PageRequest.of(Objects.nonNull(page)? page : Constants.PAGE_INIT, Constants.PAGE_SIZE);

        return movementsRepository.findAllWithPagination(pageable);
    }

}
