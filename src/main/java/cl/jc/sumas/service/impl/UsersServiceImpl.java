package cl.jc.sumas.service.impl;

import cl.jc.sumas.domain.ERole;
import cl.jc.sumas.domain.Role;
import cl.jc.sumas.domain.User;
import cl.jc.sumas.dto.SessionDto;
import cl.jc.sumas.dto.SignInDto;
import cl.jc.sumas.dto.SignUpDto;
import cl.jc.sumas.exception.BadRequestException;
import cl.jc.sumas.repository.RoleRepository;
import cl.jc.sumas.repository.UsersRepository;
import cl.jc.sumas.service.UsersService;
import cl.jc.sumas.utils.Constants;
import cl.jc.sumas.utils.JwtUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

import java.util.Date;
import java.util.HashSet;
import java.util.Set;
import java.util.stream.Collectors;

@Service
public class UsersServiceImpl implements UsersService {

    @Autowired
    UsersRepository usersRepository;

    @Autowired
    RoleRepository roleRepository;

    @Autowired
    AuthenticationManager authenticationManager;

    @Autowired
    JwtUtils jwtUtils;

    @Override
    public SessionDto createUser(SignUpDto signUpDto) throws BadRequestException {
        validateSignUp(signUpDto);
        if (usersRepository.existsByUsername(signUpDto.getUsername())) {
            throw new BadRequestException("Username is already taken");
        }

        PasswordEncoder encoder = new BCryptPasswordEncoder();

        User newUser = new User();
        newUser.setUsername(signUpDto.getUsername());
        newUser.setPassword(
                encoder.encode(
                    signUpDto.getPassword()
                )
            );
        Role userRole = roleRepository.findByName(ERole.user).orElseThrow(() -> new RuntimeException("Error: Role is not found."));
        Set<Role> roles = new HashSet<>();

        roles.add(userRole);
        newUser.setRoles(roles);

        usersRepository.save(newUser);

        return authenticate(signUpDto.getUsername(), signUpDto.getPassword());
    }

    @Override
    public SessionDto loginUser(SignInDto signInDto) throws BadRequestException {

        validateSignIn(signInDto);

        return authenticate(signInDto.getUsername(), signInDto.getPassword());
    }

    private SessionDto authenticate(String username, String password) {
        SessionDto session = new SessionDto();
        Authentication authentication = authenticationManager.authenticate(
                new UsernamePasswordAuthenticationToken(username, password));
        SecurityContextHolder.getContext().setAuthentication(authentication);

        UserDetailsImpl userDetails = (UserDetailsImpl) authentication.getPrincipal();
        session.setToken(Constants.TOKEN_PREFIX + jwtUtils.generateJwtToken(authentication));
        session.setRoles(userDetails.getAuthorities().stream()
                .map(GrantedAuthority::getAuthority)
                .collect(Collectors.toList()));
        session.setId(userDetails.getId());

        return session;
    }

    private void validateSignUp(SignUpDto signUpDto) throws BadRequestException {
        if (signUpDto.getUsername().trim().isEmpty() || signUpDto.getPassword().trim().isEmpty()) {
            throw new BadRequestException("username and password cant by empty or null");
        }
    }

    private void validateSignIn(SignInDto signInDto) throws BadRequestException {
        if (signInDto.getUsername().trim().isEmpty() || signInDto.getPassword().trim().isEmpty()) {
            throw new BadRequestException("username and password cant by empty or null");
        }
    }

}
