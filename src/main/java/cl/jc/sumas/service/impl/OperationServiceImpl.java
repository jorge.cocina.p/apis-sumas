package cl.jc.sumas.service.impl;

import cl.jc.sumas.dto.OperationResponseDto;
import cl.jc.sumas.exception.BadRequestException;
import cl.jc.sumas.service.OperationService;
import org.springframework.stereotype.Service;

import java.util.Objects;

@Service
public class OperationServiceImpl implements OperationService {

    @Override
    public OperationResponseDto addition(String a, String b) throws BadRequestException {
        if (Objects.isNull(a) || Objects.isNull(b)) {
            throw new BadRequestException("Numbers a and b can not be null");
        }
        double aLong;
        double bLong;
        try {
            aLong = Double.parseDouble(a);
            bLong = Double.parseDouble(b);
        } catch (Exception e) {
            throw new BadRequestException("Error parsing numbers");
        }

        OperationResponseDto responseDto = new OperationResponseDto();
        responseDto.setOperation(a + " + " + b);

        responseDto.setResult(Double.toString(aLong + bLong));

        return responseDto;
    }
}
