package cl.jc.sumas.service;

import cl.jc.sumas.dto.OperationResponseDto;
import cl.jc.sumas.exception.BadRequestException;

public interface OperationService {

    OperationResponseDto addition(String a, String b) throws BadRequestException;

}
