package cl.jc.sumas.service;

import cl.jc.sumas.domain.Movement;
import org.springframework.data.domain.Page;

public interface MovementService {

    void saveMovement(String output, String status);
    Page<Movement> getMovements(Integer page);
}
