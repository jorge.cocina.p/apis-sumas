package cl.jc.sumas.service;

import cl.jc.sumas.dto.SessionDto;
import cl.jc.sumas.dto.SignInDto;
import cl.jc.sumas.dto.SignUpDto;
import cl.jc.sumas.exception.BadRequestException;

public interface UsersService {

    SessionDto createUser(SignUpDto signUpDto) throws BadRequestException;

    SessionDto loginUser(SignInDto signInDto) throws BadRequestException;

}
