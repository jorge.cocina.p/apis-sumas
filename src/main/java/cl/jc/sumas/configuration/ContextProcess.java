package cl.jc.sumas.configuration;

public final class ContextProcess {

    private static final ThreadLocal<String> CALLED_URL = new ThreadLocal<>();
    private static final ThreadLocal<String> CALLED_PARAMS = new ThreadLocal<>();
    private static final ThreadLocal<String> CALLER_ID = new ThreadLocal<>();

    private ContextProcess(){}

    public static String getCalledUrl() {
        return CALLED_URL.get();
    }
    public static void setCalledUrl(String url) {
        CALLED_URL.set(url);
    }

    public static String getCalledParams() {
        return CALLED_PARAMS.get();
    }
    public static void setCalledParams(String params) {
        CALLED_PARAMS.set(params);
    }

    public static String getCallerId() {
        return CALLER_ID.get();
    }
    public static void setCallerId(String idUser) {
        CALLER_ID.set(idUser);
    }

    public static void setContextProcess(String url, String params, String idUser) {
        ContextProcess.setCalledUrl(url);
        ContextProcess.setCalledUrl(params);
        ContextProcess.setCallerId(idUser);
    }

    public static void clear() {
        CALLED_URL.remove();
        CALLED_PARAMS.remove();
        CALLER_ID.remove();
    }

}
