package cl.jc.sumas.repository;

import cl.jc.sumas.domain.Movement;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

public interface MovementsRepository extends JpaRepository<Movement, Long> {

    @Query("select m from Movement m order by createdOn desc")
    Page<Movement> findAllWithPagination(Pageable pageable);

}
