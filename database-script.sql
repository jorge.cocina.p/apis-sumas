create schema apissumas;

CREATE TABLE apissumas.users (
	id SERIAL PRIMARY KEY,
	username VARCHAR ( 50 ) UNIQUE NOT NULL,
	password text NOT NULL,
	created_on TIMESTAMP NOT NULL,
    last_login TIMESTAMP
);

create table apissumas.roles (
	id SERIAL PRIMARY KEY,
	rolename varchar(50) not null
);

create table apissumas.users_roles (
	id SERIAL PRIMARY KEY,
	id_user integer not null,
	id_role integer not null,
	CONSTRAINT fk_user
      FOREIGN KEY(id_user)
	  REFERENCES apissumas.users(id),
	CONSTRAINT fk_role
      FOREIGN KEY(id_role)
	  REFERENCES apissumas.roles(id)
);

create table apissumas.movements (
	id SERIAL PRIMARY KEY,
	username varchar(50),
	status varchar(50),
	endpoint varchar(200) not null,
	"input" varchar(200),
	"output" text,
	created_on TIMESTAMP NOT NULL
);

insert into apissumas.roles (rolename) values ('user'), ('admin');